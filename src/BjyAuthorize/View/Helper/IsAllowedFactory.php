<?php

namespace BjyAuthorize\View\Helper;

use Psr\Container\ContainerInterface;

class IsAllowedFactory
{
    /**
     *
     * @param ContainerInterface $container
     *
     * @return IsAllowed
     */
    public function __invoke(ContainerInterface $container, $requestedName, $options = null)
    {
        /* @var $authorize \BjyAuthorize\Service\Authorize */
        $authorize = $container->get('BjyAuthorize\Service\Authorize');

        return new IsAllowed($authorize);
    }

}