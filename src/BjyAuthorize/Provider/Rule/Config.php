<?php

namespace BjyAuthorize\Provider\Rule;

/**
 * Rule provider based on a given array of rules
 *
 * @author Ben Youngblood <bx.youngblood@gmail.com>
 */
class Config implements ProviderInterface
{
    /**
     * @var array
     */
    protected $rules = array();

    /**
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        $this->rules = $config;
    }

    /**
     * {@inheritDoc}
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->rules = $config;
    }

    /**
     * @return mixed
     */
    public function processConfig()
    {
        // nop
    }
}
