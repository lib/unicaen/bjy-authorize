<?php

namespace BjyAuthorize\Guard;

use BjyAuthorize\Provider\Rule\ProviderInterface as RuleProviderInterface;
use BjyAuthorize\Provider\Resource\ProviderInterface as ResourceProviderInterface;
use \Laminas\EventManager\AbstractListenerAggregate;
use Laminas\ServiceManager\ServiceLocatorInterface;

abstract class AbstractGuard extends AbstractListenerAggregate implements
    GuardInterface,
    RuleProviderInterface,
    ResourceProviderInterface
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var array[]
     */
    protected $rules = array();

    /**
     *
     * @param array                   $config
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(array $config, ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;

        $this->setConfig($config);
    }

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @param array $config
     */
    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    /**
     * @return void
     */
    public function processConfig()
    {
        $this->rules = $this->makeRules();
    }

    /**
     * @return array
     */
    protected function makeRules()
    {
        $rules = [];

        foreach ($this->config as $rule) {
            $rule['roles']  = isset($rule['roles']) ? (array) $rule['roles'] : array();
            $rule['action'] = isset($rule['action']) ? (array) $rule['action'] : array(null);

            foreach ($this->extractResourcesFromRule($rule) as $resource) {
                $rules[$resource] = array('roles' => (array) $rule['roles']);

                if (isset($rule['assertion'])) {
                    $rules[$resource]['assertion'] = $rule['assertion'];
                }
            }
        }

        return $rules;
    }

    abstract protected function extractResourcesFromRule(array $rule);

    /**
     * {@inheritDoc}
     */
    public function getResources()
    {
        $resources = array();

        foreach (array_keys($this->rules) as $resource) {
            $resources[] = $resource;
        }

        return $resources;
    }

    /**
     * {@inheritDoc}
     */
    public function getRules()
    {
        $rules = array();
        foreach ($this->rules as $resource => $ruleData) {
            $rule   = array();
            $rule[] = (array) ($ruleData['roles'] ?? null);
            $rule[] = $resource;

            if (isset($ruleData['assertion'])) {
                $rule[] = null; // no privilege
                $rule[] = $ruleData['assertion'];
            }

            $rules[] = $rule;
        }

        return array('allow' => $rules);
    }
}
