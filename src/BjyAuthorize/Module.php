<?php

namespace BjyAuthorize;

use Laminas\EventManager\AbstractListenerAggregate;
use Laminas\EventManager\EventInterface;
use Laminas\ModuleManager\Feature\AutoloaderProviderInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\View\Helper\Navigation;

/**
 * BjyAuthorize Module
 *
 * @author Ben Youngblood <bx.youngblood@gmail.com>
 */
class Module implements
    AutoloaderProviderInterface,
    BootstrapListenerInterface,
    ConfigProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function onBootstrap(EventInterface $event)
    {
        if (self::inConsole()) {
            // Pas de lancement de BjyAuthorize en mode console
            return;
        }

        /* @var $app \Laminas\Mvc\ApplicationInterface */
        $app            = $event->getTarget();
        /* @var $sm \Laminas\ServiceManager\ServiceLocatorInterface */
        $serviceManager = $app->getServiceManager();
        $config         = $serviceManager->get('BjyAuthorize\Config');
        $strategy       = $serviceManager->get($config['unauthorized_strategy']);
        $guards         = $serviceManager->get('BjyAuthorize\Guards');

        // transmission des ACL aux aides de vue de navigation
        $authorizeService = $serviceManager->get('BjyAuthorize\Service\Authorize');
        /* @var $authorizeService \BjyAuthorize\Service\Authorize */

        Navigation::setDefaultAcl($authorizeService->getAcl());
        Navigation::setDefaultRole($authorizeService->getIdentity());
        foreach ($guards as $guard) {
            /** @var AbstractListenerAggregate $guard */
            $guard->attach($app->getEventManager());
        }

        /** @var AbstractListenerAggregate $strategy */
        $strategy->attach($app->getEventManager());
    }

    /**
     * {@inheritDoc}
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Laminas\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }



    public static function inConsole(): bool
    {
        return PHP_SAPI == 'cli';
    }
}
