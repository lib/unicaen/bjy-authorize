<?php

namespace BjyAuthorize\Service;

use BjyAuthorize\Provider\Rule\ProviderInterface;
use Psr\Container\ContainerInterface;

/**
 * Base factory responsible of instantiating providers
 */
abstract class BaseProvidersServiceFactory
{
    const PROVIDER_SETTING = 'providers';

    public function __invoke(ContainerInterface $container)
    {
        $config    = $container->get('BjyAuthorize\Config');
        $providers = array();

        foreach ($config[static::PROVIDER_SETTING] as $providerName => $providerConfig) {
            if ($container->has($providerName)) {
                $provider = $container->get($providerName);
                if ($provider instanceof ProviderInterface) {
                    $provider->setConfig($providerConfig);
                    $provider->processConfig();
                }
                $providers[] = $provider;
            } else {
                $providers[] = new $providerName($providerConfig, $container);
            }
        }

        return $providers;
    }
}
