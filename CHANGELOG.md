CHANGELOG
=========

6.1.4 (29/01/2025)
------------------

- Montée en version de ZfcUser pour retrait dépendance laminas-crypt

6.1.3
-----
- Mise à jour du gitlab-ci pour lancer correctement la MAJ avec Satis

6.1.2
-----
- Module désactivé en mode Console : config neutralisée & isAllowed renvoie true

6.1.1
-----
- Retrait de la dépendance à laminas-dependency-plugin

6.1.0
-----
- Compatible PHP8.0 et PHP8.2

5.0.0
-----
- PHP 8 requis.

4.0.1
-----
- [FIX] Pas cool de bloquer les bibliothèques unicaen à la version 4.0.0 !
- PHP 7.4 requis

4.0.0
-----
- Changelog ;)
- Passage de Zend à Laminas