<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link           http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright      Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license        http://framework.zend.com/license/new-bsd New BSD License
 * @package        Zend_Service
 */

namespace BjyAuthorizeTest\Service;

use PHPUnit\Framework\TestCase;
use BjyAuthorize\Service\AuthorizeAwareServiceInitializer;

/**
 * Test for {@see \BjyAuthorize\Service\AuthorizeAwareServiceInitializer}
 *
 * @author Marco Pivetta <ocramius@gmail.com>
 */
class AuthorizeAwareServiceInitializerTest extends TestCase
{
    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $authorize;

    /**
     * @var \PHPUnit\Framework\MockObject\MockObject
     */
    protected $locator;

    /**
     * @var \BjyAuthorize\Service\AuthorizeAwareServiceInitializer
     */
    protected $initializer;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        $this->authorize   = $this->createMock('BjyAuthorize\\Service\\Authorize'/*, array(), array(), '', false*/);
        $this->locator     = $this->createMock('Laminas\\ServiceManager\\ServiceLocatorInterface');
        $this->initializer = new AuthorizeAwareServiceInitializer();

        $this->locator->expects($this->any())->method('get')->will($this->returnValue($this->authorize));
    }

    /**
     * @covers \BjyAuthorize\Service\AuthorizeAwareServiceInitializer::initialize
     */
    public function testInitializeWithAuthorizeAwareObject()
    {
        $awareObject = $this->createMock('BjyAuthorize\\Service\\AuthorizeAwareInterface');

        $awareObject->expects($this->once())->method('setAuthorizeService')->with($this->authorize);

        $this->initializer->initialize($awareObject, $this->locator);
    }

    /**
     * @covers \BjyAuthorize\Service\AuthorizeAwareServiceInitializer::initialize
     */
    public function testInitializeWithSimpleObject()
    {
        $awareObject = $this->createMock('stdClass'/*, array('setAuthorizeService')*/);

        $awareObject->expects($this->never())->method('setAuthorizeService');

        $this->initializer->initialize($awareObject, $this->locator);
    }
}
